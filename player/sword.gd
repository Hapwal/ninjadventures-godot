extends Area2D

@onready var shape = $hitBox

func enable():
	shape.disabled = false

func disable():
	shape.disabled = true
