extends Camera2D

@export var tilemap: TileMap

@export var randomStrength: float = 30.0
@export var shakeFade: float = 5.0

var rng = RandomNumberGenerator.new()
var shake_strength: float = 0.0

func apply_shake():
	shake_strength = randomStrength
	

# Called when the node enters the scene tree for the first time.
func _ready():
	#var mapRect = tilemap.get_used_rect()
	#var tileSize = tilemap.rendering_quadrant_size
	#var worldSizeInPixels = mapRect.size * tileSize
	#limit_right = worldSizeInPixels.x
	#limit_bottom = worldSizeInPixels.y
	#print(limit_right) #1584 -> 1220
	#print(limit_bottom) # 768 -> 450
	
	var visibleArea = tilemap.get_used_rect()
	var tileSize = tilemap.cell_quadrant_size
	var _upperLeftCorner = visibleArea.position * tileSize
	var _lowerRightCorner = (visibleArea.position + visibleArea.size) * tileSize
	limit_left = tilemap.position.x + _upperLeftCorner.x
	limit_top = tilemap.position.y + _upperLeftCorner.y
	limit_right = tilemap.position.x + _lowerRightCorner.x
	limit_bottom = tilemap.position.y + _lowerRightCorner.y

	#print(tilemap.position.x)
	#print(tilemap.position.y)
	#print(limit_right)
	#print(limit_bottom)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
	#testing camerashake begining
	#if Input.is_action_just_pressed("shake"):
		#apply_shake()
		#
	#if shake_strength > 0:
		#shake_strength = lerpf(shake_strength,0,shakeFade*_delta)
		#offset = randomOffset()
	#testing camerashake ending

func test():
	print_debug("ooooh")

func handleShake():
	apply_shake()
	if shake_strength > 0:
		shake_strength = lerpf(shake_strength,0,shakeFade*0.06944)
		print_debug("ah")
		offset = randomOffset()

func randomOffset() -> Vector2:
	return Vector2(rng.randf_range(-shake_strength, shake_strength),rng.randf_range(-shake_strength,shake_strength))
