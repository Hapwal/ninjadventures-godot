extends CharacterBody2D

class_name Player

signal healthChanged

@export var speed: int = 100
@onready var animations = $AnimationPlayer
@onready var effects = $Effects
@onready var hurtTimer = $hurtTimer
@onready var camera = $followCam
@onready var hurtBox = $HurtBox
@onready var weapon = $weapon

@export var maxHealth = 3 
@onready var currentHealth: int = maxHealth

@export var knockbackPower: int = 500

@export var inventory: Inventory

var lastAnimDirection: String = "Down"
var isHurt: bool = false
var isAttacking: bool = false



#func _ready():
	#$Sprite2D.frame = 0

func _ready():
	effects.play("RESET")
	weapon.disable()
	#var weaponChild = weapon.get_children()
	#var weaponChildHitbox = weaponChild[0].get_node("hitBox")
	#weaponChildHitbox.disabled = true

func handleInput():
	var moveDirection = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
	velocity = moveDirection * speed
	
	if Input.is_action_just_pressed("attack"):
		attack()

func attack():
	#var weaponChild = $weapon.get_children()
	#var weaponChildHitbox = weaponChild[0].get_node("hitBox")
	animations.play("attack"+ lastAnimDirection)
	#weaponChildHitbox.disabled = false
	isAttacking = true
	weapon.enable()
	await animations.animation_finished
	#weaponChildHitbox.disabled = true
	weapon.disable()
	isAttacking = false

func updateAnimation():
	if isAttacking: return
	
	if velocity.length() == 0:
		if animations.is_playing():
			animations.stop()
	else:
		var direction = "Down"
		if velocity.x < 0: direction = "Left"
		elif velocity.x > 0: direction = "Right"
		elif velocity.y < 0: direction = "Up"
		animations.play("walk" + direction)
		lastAnimDirection = direction
		
#func handleCollision():
	#for i in get_slide_collision_count():
		#var collision = get_slide_collision(i)
		#var collider = collision.get_collider()
		#print_debug(collider.name)

func _physics_process(_delta):
	#handleCollision()
	handleInput()
	move_and_slide()
	updateAnimation()
	if !isHurt:
		for area in hurtBox.get_overlapping_areas():
			if area.name == "HitBox":
				hurtByEnemy(area)
	
func hurtByEnemy(area):
	currentHealth -= 1 
	if currentHealth < 0:
		currentHealth = maxHealth
	
	healthChanged.emit(currentHealth)
	isHurt = true
	#camera.handleShake()
	knockback(area.get_parent().velocity)
	effects.play("hurtBlink")
	hurtTimer.start()
	await hurtTimer.timeout
	effects.play("RESET")
	isHurt = false

func _on_hurt_box_area_entered(area):
	if area.is_in_group("collectables"):
		#if area.has_method("collect"):
		area.collect(inventory)
		

func knockback(enemyVelocity: Vector2):
	var knockbackDirection = (enemyVelocity - velocity).normalized() * knockbackPower
	velocity = knockbackDirection
	move_and_slide()


#func _on_hurt_box_area_exited(area):pass
