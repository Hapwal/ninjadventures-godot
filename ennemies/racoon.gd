extends CharacterBody2D

#@export var speed = 40
@export var limit = 20
@export var endPoint: Marker2D

@onready var animations = $AnimatedSprite2D

var speed = randf_range(20, 50)
var value = randf_range(-1, 1)
var direction = Vector2(value,value).normalized()
var speed_variation_rate = 0.5
var direction_variation_rate = 0.5

var startPosition
var endPosition

func _ready():
	startPosition = position
	#endPosition = startPosition + Vector2(0, 3*16)
	#endPosition = endPoint.global_position #it is the actual position in the game
	
	var val1 = randf_range(0, get_viewport().size.x)
	var val2 = randf_range(0, get_viewport().size.y)
	endPosition = Vector2(val1, val2)


func randomize_speed_and_direction():
	if randf() < speed_variation_rate:
		speed = randf_range(50, 150)

	if randf() < direction_variation_rate:
		direction = Vector2(value, value).normalized()

func changeDirection():
	var tempEnd = endPosition
	endPosition = startPosition
	startPosition = tempEnd

func updateVelocity():
	var moveDirection = (endPosition - position)
	if moveDirection.length() < limit:
		changeDirection()
	velocity = moveDirection.normalized()*speed
	

func updateAnimation():
	var animationString = "walkUp"
	if velocity.y >  0:
		animationString = "walkDown"
		
	animations.play(animationString)

func _physics_process(_delta):
	updateVelocity()
	move_and_slide()
	updateAnimation()
	
	randomize_speed_and_direction()
	position += direction * speed * _delta
