extends CharacterBody2D

@export var speed = 40
@export var limit = 20
@export var endPoint: Marker2D

#@onready var animations = $AnimatedSprite2D
@onready var animations = $AnimationPlayer

var isDead: bool = false

var startPosition
var endPosition

func _ready():
	startPosition = position
	#endPosition = startPosition + Vector2(0, 3*16)
	endPosition = endPoint.global_position #it is the actual position in the game

func changeDirection():
	var tempEnd = endPosition
	endPosition = startPosition
	startPosition = tempEnd

func updateVelocity():
	var moveDirection = (endPosition - position)
	if moveDirection.length() < limit:
		changeDirection()
	velocity = moveDirection.normalized()*speed
	

func updateAnimation():
	if velocity.length() == 0:
		if animations.is_playing():
			animations.stop()
	else:
		var direction = "Down"
		if velocity.x < 0: direction = "Left"
		elif velocity.x > 0: direction = "Right"
		elif velocity.y < 0: direction = "Up"
		animations.play("walk" + direction)

func _physics_process(_delta):
	if isDead: return
	updateVelocity()
	move_and_slide()
	updateAnimation()


func _on_hurt_box_area_entered(area):
	if area == $hitBox: return
	if area.is_in_group("meleeWeapon"):
		$hitBox.set_deferred("monitorable", false)#attend la fin de la frame 
		isDead = true
		animations.play("death")
		await animations.animation_finished
		queue_free()
		
	
